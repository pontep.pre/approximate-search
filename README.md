# Docker-Approximate-Search

## Install prerequisites
ก่อนที่จะติดตั้งระบบให้ตรวจสอบให้แน่ใจว่าได้ทำการติดตั้งซอฟต์แวร์ ดังนี้
* [docker](https://docs.docker.com/engine/install/)
* [golang](https://golang.org/dl/)

## Installation
**Clone Repository**
```sh 
git clone https://gitlab.com/pontep.pre/approximate-search.git
```
**Change Directory**
```sh 
cd approximate-search
```

## Usage
**Creating New go.mod**
```sh 
go mod init app
```
**Run Container**
```sh 
docker-compose up -d
```
**Console Container**
```sh
docker exec -it <CONTAINER_ID> bash
```

## Endpoints 

**GET** http://localhost:8980/
-  API 

**GET** http://localhost:8983/
- Solr Database
